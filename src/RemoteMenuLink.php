<?php

declare(strict_types=1);

namespace Drupal\menu_parser_php;

use Drupal\Core\Menu\Form\MenuLinkDefaultForm;
use Drupal\Core\Menu\MenuLinkBase;
use Drupal\Core\Menu\MenuLinkDefault;

/**
 * Represents a remote menu link item.
 */
class RemoteMenuLink extends MenuLinkBase {

  /**
   * Creates a new RemoteMenuLink object.
   *
   * @param string $link
   *   The menu link path.
   * @param string $title
   *   The menu link title.
   * @param int $weight
   *   The menu link weight.
   * @param string $id
   *   The plugin ID.
   *
   * @return static
   *   The created object.
   */
  public static function fromLink(string $link, string $title, int $weight, string $id): static {
    return new static(
      [],
      'remote_menu_link:' . $id,
      [
        'menu_name' => 'remote',
        'parent' => '',
        'url' => $link,
        'options' => [],
        'title' => $title,
        'class' => MenuLinkDefault::class,
        'provider' => 'remote_menu_link',
        'enabled' => TRUE,
        'expanded' => TRUE,
        'weight' => $weight,
        'form_class' => MenuLinkDefaultForm::class,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function updateLink(array $new_definition_values, $persist) {}

}

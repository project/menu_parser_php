<?php

declare(strict_types=1);

namespace Drupal\menu_parser_php;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;

/**
 * Parses a remote menu JSON linkset.
 */
class RemoteMenuParser {

  use StringTranslationTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * A custom cache bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBin;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * RemoteMenuParser constructor.
   */
  public function __construct(
    Client $client,
    ConfigFactoryInterface $config_factory,
    CacheBackendInterface $cache,
    TimeInterface $time
  ) {
    $this->httpClient = $client;
    $this->config = $config_factory->get('menu_parser_php.settings');
    $this->cacheBin = $cache;
    $this->time = $time;
  }

  /**
   * Parses a remote menu.
   *
   * @param string $url
   *   The URL at which the JSON linkset is available.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface|null $metadata
   *   Optional cache metadata.
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]|null
   *   A tree of MenuLinkTreeElement objects. NULL on failure.
   */
  public function readRemoteMenu(string $url, RefinableCacheableDependencyInterface $metadata = NULL): ?array {
    $url_without_http_auth = static::removeHttpAuth($url);
    $cached = $this->cacheBin->get($url_without_http_auth);
    if ($cached) {
      if ($metadata) {
        $tags = $cached->tags ?? [];
        $expire = $cached->expire ?? Cache::PERMANENT;
        $metadata->addCacheTags($tags);
        $max_age = $expire - $this->time->getCurrentTime();
        $metadata->mergeCacheMaxAge($max_age);
      }
      return $cached->data;
    }

    try {
      $json = $this->httpClient->get($url)->getBody()->getContents();
      $json_parsed = json_decode($json, FALSE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\Exception $e) {
      return NULL;
    }
    $data = [];
    // Convert a flat JSON into a tree array.
    foreach ($this->flattenReceivedJson($json_parsed) as $item) {
      $location = static::parseLocation($item);
      $link_href = $item->href ?? '';
      $link_title = $item->title ?? (string) $this->t('- Missing Link Title -');
      $data = static::insertLink($location, $link_href, $link_title, $data);
    }
    // Convert the tree array into a tree of RemoteMenuLink objects.
    $base_url = parse_url($url, PHP_URL_SCHEME) . '://' . parse_url($url, PHP_URL_HOST);
    $base_url = trim($base_url, '/');
    static::convertToObjects($data, $base_url);

    $expire_time_config = $this->config->get('cache_expire') ?? 24 * 60 * 60;
    $expire_time_config = (int) $expire_time_config;
    if ($expire_time_config === CacheBackendInterface::CACHE_PERMANENT) {
      $expire = CacheBackendInterface::CACHE_PERMANENT;
    }
    else {
      $expire = $this->time->getCurrentTime() + $expire_time_config;
    }
    $cache_tags = [static::getCacheTag($url_without_http_auth)];
    $this->cacheBin->set($url_without_http_auth, $data, $expire, $cache_tags);
    if ($metadata) {
      $metadata->mergeCacheMaxAge($expire_time_config);
      $metadata->addCacheTags($cache_tags);
    }
    return $data;
  }

  /**
   * Extracts all menu links from the received JSON.
   *
   * @param \stdClass $json
   *   The JSON object.
   *
   * @return array<object>
   *   An array of link objects.
   */
  protected function flattenReceivedJson(\stdClass $json): array {
    $output = [];
    foreach ($json->linkset[0] as $item) {
      if (is_object($item)) {
        $output[] = $item;
        continue;
      }
      if (!is_array($item)) {
        continue;
      }
      foreach ($item as $subitem) {
        if (is_object($subitem)) {
          $output[] = $subitem;
        }
      }
    }
    return $output;
  }

  /**
   * Converts the tree array into a tree of RemoteMenuLink objects.
   *
   * @param array $input
   *   The tree of links.
   * @param string $base_url
   *   The base site url of the remote links.
   */
  protected static function convertToObjects(array &$input, string $base_url): void {
    foreach ($input as &$item) {
      $weight = 0;
      if (array_key_exists('subtree', $item)) {
        static::convertToObjects($item['subtree'], $base_url);
      }
      else {
        $item['subtree'] = [];
      }
      $full_link = $base_url . $item['link'];
      $link = RemoteMenuLink::fromLink($full_link, $item['title'], $weight, $item['location']);
      $has_children = !empty($item['subtree']);
      $depth = mb_substr_count($item['location'], '.');
      $item = new MenuLinkTreeElement($link, $has_children, $depth, FALSE, $item['subtree']);
    }
  }

  /**
   * Creates a tree leaf. The leaf is an array.
   *
   * @param string $location
   *   The linkset item location.
   * @param string $link
   *   The menu link path.
   * @param string $title
   *   The menu link title.
   * @param array $data
   *   The tree into which the leaf will be inserted.
   *
   * @return array
   *   The adjusted tree.
   */
  protected static function insertLink(string $location, string $link, string $title, array $data): array {
    $pointer = &$data;
    $fragments = array_filter(explode('.', $location), 'strlen');
    // Traverse the tree.
    while (TRUE) {
      if (count($fragments) === 1) {
        // This is not the final child.
        $loc = reset($fragments);
        // Set the data of the child.
        $pointer[$loc]['link'] = $link;
        $pointer[$loc]['title'] = $title;
        $pointer[$loc]['location'] = $location;
        break;
      }
      // Shift out the topmost parent.
      $loc = array_shift($fragments);
      // Move the pointer into the child.
      $pointer = &$pointer[$loc]['subtree'];
    }
    return $data;
  }

  /**
   * Parses the location string.
   *
   * @param object $item
   *   The menu item object.
   *
   * @return string
   *   The location string.
   */
  protected static function parseLocation(object $item): string {
    if (property_exists($item, 'drupal-menu-hierarchy')) {
      // This is the behaviour in contrib module decoupled_menus.
      return $item->{'drupal-menu-hierarchy'}[0];
    }
    // This is the behaviour in 10.1 and onwards.
    assert(property_exists($item, 'hierarchy'));
    $hierarchy_array = $item->hierarchy;
    return '.' . implode('.', $hierarchy_array);
  }

  /**
   * Generates a custom cache tag for a remote menu URL.
   *
   * @param string $url
   *   The URL.
   *
   * @return string
   *   The cache tag name.
   */
  public static function getCacheTag(string $url): string {
    $url = static::removeHttpAuth($url);
    $hash = substr(hash('sha256', $url), 30);
    return 'menu_parser_php:menu:' . $hash;
  }

  /**
   * Removes http auth details from a URL.
   *
   * @param string $url
   *   The URL to clean.
   *
   * @return string
   *   The resulting cleaned URL.
   */
  private static function removeHttpAuth(string $url): string {
    return preg_replace('#/[^/]*@#', '/', $url);
  }

}

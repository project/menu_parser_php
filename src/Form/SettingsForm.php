<?php

declare(strict_types=1);

namespace Drupal\menu_parser_php\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Module configuration form.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('menu_parser_php.settings');
    $form['cache_expire'] = [
      '#type' => 'number',
      '#min' => -1,
      '#title' => $this->t('Cache entry expire'),
      '#description' => $this->t('Cache entry expire time in seconds. Use -1 for permanent.'),
      '#default_value' => $config->get('cache_expire'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('menu_parser_php.settings');
    $config->set('cache_expire', $form_state->getValue('cache_expire'));
    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['menu_parser_php.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_parser_php_settings';
  }

}

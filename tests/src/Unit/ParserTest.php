<?php

declare(strict_types=1);

namespace Drupal\Tests\menu_parser_php\Unit;

use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\menu_parser_php\RemoteMenuParser;
use Drupal\Tests\UnitTestCase;

/**
 * Unit test class base.
 *
 * @coversDefaultClass \Drupal\menu_parser_php\RemoteMenuParser
 */
class ParserTest extends UnitTestCase {

  /**
   * @covers \Drupal\menu_parser_php\RemoteMenuParser::insertLink
   */
  public function testInsertLinks(): void {
    $call_method = static function ($location, $link, $title, $data): array {
      $class = new \ReflectionClass(RemoteMenuParser::class);
      $method = $class->getMethod('insertLink');
      $method->setAccessible(TRUE);
      return $method->invokeArgs(NULL, [$location, $link, $title, $data]);
    };

    $data = [];

    // Insert and check a top level item.
    $data = $call_method('.000', '/test0', 'Title .000', $data);
    static::assertArrayHasKey('000', $data);
    $expected = [
      'link' => '/test0',
      'title' => 'Title .000',
      'location' => '.000',
    ];
    static::assertEqualsCanonicalizing($expected, $data['000']);

    // Insert and check another top level item.
    $data = $call_method('.001', '/test1', 'Title .001', $data);
    static::assertArrayHasKey('001', $data);
    $expected = [
      'link' => '/test1',
      'title' => 'Title .001',
      'location' => '.001',
    ];
    static::assertEqualsCanonicalizing($expected, $data['001']);

    // Insert and check a second level item.
    $data = $call_method('.002.000', '/test2', 'Title .002.000', $data);
    static::assertArrayHasKey('002', $data);
    static::assertArrayHasKey('subtree', $data['002']);
    static::assertArrayHasKey('000', $data['002']['subtree']);
    $expected = [
      'link' => '/test2',
      'title' => 'Title .002.000',
      'location' => '.002.000',
    ];
    static::assertEqualsCanonicalizing($expected, $data['002']['subtree']['000']);

    // Insert and check another top level item.
    $data = $call_method('.002', '/test3', 'Title .002', $data);
    $expected = [
      'link' => '/test3',
      'title' => 'Title .002',
      'location' => '.002',
      'subtree' => [
        '000' => $expected,
      ],
    ];
    static::assertEqualsCanonicalizing($expected, $data['002']);

    // Insert and check a second level item.
    $data = $call_method('.000.000', '/test4', 'Title .000.000', $data);
    static::assertArrayHasKey('subtree', $data['000']);
    static::assertArrayHasKey('000', $data['000']['subtree']);
    $expected = [
      'link' => '/test0',
      'title' => 'Title .000',
      'location' => '.000',
      'subtree' => [
        '000' => [
          'link' => '/test4',
          'title' => 'Title .000.000',
          'location' => '.000.000',
        ],
      ],
    ];
    static::assertEqualsCanonicalizing($expected, $data['000']);
  }

  /**
   * @covers \Drupal\menu_parser_php\RemoteMenuParser::convertToObjects
   */
  public static function testObjectCreation(): void {
    // Create some sample output as ::insertLink would return it.
    $data = [
      '000' => [
        'link' => '/test0',
        'title' => 'Title .000',
        'location' => '.000',
      ],
      '001' => [
        'link' => '/test1',
        'title' => 'Title .001',
        'location' => '.001',
        'subtree' => [
          '000' => [
            'link' => '/test0',
            'title' => 'Title .001.000',
            'location' => '.001.000',
          ],
          '001' => [
            'link' => '/test2',
            'title' => 'Title .001.001',
            'location' => '.001.001',
            'subtree' => [
              '000' => [
                'link' => '/test3',
                'title' => 'Title .001.001.000',
                'location' => '.001.001.000',
              ],
            ],
          ],
        ],
      ],
    ];
    // Call ::convertToObjects.
    $class = new \ReflectionClass(RemoteMenuParser::class);
    $method = $class->getMethod('convertToObjects');
    $method->setAccessible(TRUE);
    $method->invokeArgs(NULL, [&$data, 'https://site.com']);

    // Check the top level items.
    static::assertArrayHasKey('000', $data);
    static::assertArrayHasKey('001', $data);
    static::assertCount(2, $data);

    // Check second level children.
    static::assertEquals(1, $data['001']->depth);
    static::assertTrue($data['001']->hasChildren);
    static::assertArrayHasKey('000', $data['001']->subtree);
    static::assertArrayHasKey('001', $data['001']->subtree);

    // Check third level item.
    static::assertArrayHasKey('000', $data['001']->subtree['001']->subtree);
    $deepest_item = $data['001']->subtree['001']->subtree['000'];
    static::assertInstanceOf(MenuLinkTreeElement::class, $deepest_item);
    static::assertEquals(3, $deepest_item->depth);
    $url = $deepest_item->link->getUrlObject()->toUriString();
    static::assertStringEndsWith('/test3', $url);
  }

}
